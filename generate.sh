#!/bin/bash
set -e
set -x
PACKAGES="$(jq -r '.[]' < packages.json)"

for package in ${PACKAGES}; do
    source=$(apt-cache show "${package}" | grep '^Source' | awk '{print $2}')
    source=${source:-$package}
    cifile="public/${source}.yml"
    cp ci-template.yml "${cifile}"
    : "INFO: reverse dependencies for ${package}:"
    : build-rdeps --only-main -q "${package}"
    for dep in $(build-rdeps --only-main -q "${package}"); do
	: pkg $dep
        cat >> "${cifile}" << _END_

build-rdep-${dep}:
  variables:
    REVERSE_DEP: ${dep}
  extends: .rdep-build-definition

_END_

    done
done

